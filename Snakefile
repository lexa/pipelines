rule align:
    input:
        "{name}.fa"
    output:
        "{name}.fasta"
    run:
        shell("clustalw -OUTPUT=FASTA -INFILE={input}")
        shell("rm {wildcards.name}.dnd")

rule consensi:
    input:
        "{name}.fasta"
    output:
        "{name}.fasta_cons.fa"
    shell:
        "em_cons -sequence {input} -outseq {output}"
