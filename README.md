# pipelines

## Minimal pipeline examples for IV110/IV114 at FI MUNI

### Dependencies

- make
- nextflow
- snakemake
- clustalw
- EMBOSS cons (as em_cons command)
- R
- R/Bioconductor seqLogo and Biostrings packages

### Instructions to run examples

make all

nextflow run main.nf

snakemake -p set{1,2,3}.fasta_cons.fa

