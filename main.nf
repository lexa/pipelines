#!/usr/bin/env nextflow

input = Channel.fromPath('*.fa')

process ALIGN {

  publishDir "/home/lexa/pipelines"
  
  input:
  file x from input
  
  output:
  file '*.fasta' into alignment
  
  script:
  """
  clustalw -OUTPUT=FASTA -INFILE=${x}
  rm *.dnd
  """
}

process GET_CONSENSUS {

  publishDir "/home/lexa/pipelines"
  
  input:
  file a from alignment

  output:
  file '*_cons.fa'

  """
  em_cons -sequence ${a} -outseq ${a}_cons.fa 
  """
}
