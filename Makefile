alignments = set1.fasta set2.fasta set3.fasta
consensi = set1.fasta_cons.fa set2.fasta_cons.fa set3.fasta_cons.fa
logos = set1.png set2.png set3.png

%.fasta: %.fa
	clustalw -OUTPUT=FASTA -INFILE=$<
	rm *.dnd

%.fasta_cons.fa: %.fasta
	em_cons -sequence $< -outseq $<_cons.fa

%.png: %.fasta
	./make_seqlogo.R $<

all: $(alignments) $(consensi) $(logos)

clean:
	rm *.fasta *_cons.fa
